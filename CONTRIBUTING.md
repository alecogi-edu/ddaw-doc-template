# Contribuyendo al proyecto

- El siguiente documento representa las normas a seguir por todos los desarrolladores.
- La comunicación se llevará a cabo a través del siguiente [canal de slack](https://app.slack.com/client/T0104RVLKL7/C01056N7WGN)

## Reporte de problemas y gestión de nuevas funcionalidades a desarrollar

- Las gestión de tareas se lleva a cabo a través de la aplicación Trello. [Tablero](https://trello.com/b/t1cIemYv/backlog-de-producto).
- Todos los días se llevará a cabo una reunión antes de empezar a trabar que quedará registrada en el siguiente [cronograma](https://gitlab.com/alecogi-edu/fct-no-hagamos-colas/-/wikis/Cronograma---Reuniones-diarias) 

## Pull Requests

- **Guía de estilo de codificación.** Deberás seguir las [reglas](https://standardjs.com/rules.html)
- **Tests**. Los desarrollos que realices deberán estar cubiertos mínimamente por test funcionales y/o unitarios
- **Documentacion**. Si tu código cambia alguna interfaz, recurso o añade nuevas características deberás actualizar La documentación asociada.

### Nombrado,  uso de ramas

- Se mantendrán 2 ramas de largo recorrido:
  - La rama **master**: Corresponde a versiones publicadas
  - La rema **desarrollo**: Corresponde a versiones en desarrollo pendientes de validación
  
- Las 2 ramas de largo recorrido serán gestionadas por la figura del **integration manager** que verificará que se cumplen todos
los requisitos para que tu trabajo sea incorporado al proyecto.
  
- Cualquier **historia de usuario** que se vaya a desarrollar se llevará a cabo en una rama, cuyo nombre coincidirá con el de la tarea
    - Las subtareas asociadas a una historia de usuario, se desarrollaran en ramas asociadas a la rama correspondiente de la historia de usuario.

### Mensajes de confirmación de cambios

- Los mensajes de confirmación de cambios deben ser descriptivos atendiendo a las siguientes indicaciones
   - **[ADD] descripción:** Hemos añadido una nueva clase, funcionalidad,...
   - **[MOD] descripción:** Se ha modificado una clase, funcionalidad,...
   - **[DEL] descripción:** Se ha borrado una clase, funcionalidad,...